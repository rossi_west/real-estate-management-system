import random


from rest_framework.serializers import ModelSerializer

from accounts.models  import Institute

from .models import InstituteListing



class InstituteSerializer(ModelSerializer):
    class Meta:
        model = InstituteListing
        exclude = ['institute_id', 'created', 'order_uuid']

    def create(self, validated_data):
        request = self.context.get('request', None)
        if hasattr(request, 'user') and request is not None:
            user = request.user
            institute_info = self.get_institute_info(user)
            create = InstituteListing.objects.create(institute_id=user, order_uuid=institute_info['order_id'], **validated_data)
            return create

    def get_institute_info(self, data):
        try:
            user = Institute.objects.get(user=data)
        except Institute.DoesNotExist:
            pass
        else:
            return {
                'order_id': user.order_id
            }