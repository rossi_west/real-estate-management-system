from django.contrib.gis.db import models
from django.contrib.postgres.fields import  JSONField
from django.conf import settings

# Create your models here.
class InstituteListing(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    institute_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    description = models.TextField()
    order_uuid = models.CharField(max_length=250)
    class_name = models.CharField(max_length=250, default='')
    
    data = JSONField()


    class Meta:
        ordering = ['-created']

    


    def __str__(self):
        return self.description


