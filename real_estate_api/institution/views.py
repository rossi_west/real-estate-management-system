from django.shortcuts import render

# Create your views here.
from rest_framework.generics import (CreateAPIView, RetrieveUpdateAPIView,
RetrieveDestroyAPIView, ListAPIView
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication

from .models import  InstituteListing
from .serializer import InstituteSerializer
from .permission import IsInstitute
from accounts.authentication import JwtAuthentication

from rest_framework.permissions import IsAuthenticated
from owner.views import PropertyListingCreateAPIView



class CreateHotelPost(CreateAPIView):
    permission_classes = (IsAuthenticated, IsInstitute
    
    )
    
    queryset = InstituteListing
    serializer_class = InstituteSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context())
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)
        return Response({'reason': serializer.errors, 'res': False, 'message': 'False'}, status=status.HTTP_200_OK)

 

class RetrieveHotelPost(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsInstitute
    
    )
    

    queryset = InstituteListing
    serializer_class = InstituteSerializer
    lookup_url_kwarg = 'pk'



class DeleteHotelPost(RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated, IsInstitute
    
    ,)
    
    queryset = InstituteListing
    serializer_class = InstituteSerializer
    lookup_url_kwarg = 'pk'

class ListHotelPost(ListAPIView):
    permission_classes = (IsAuthenticated, IsInstitute
    
    ,)
    
    queryset = InstituteListing
    serializer_class = InstituteSerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(institute_id=user).all()
