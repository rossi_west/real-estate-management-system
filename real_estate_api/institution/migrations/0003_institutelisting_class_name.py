# Generated by Django 2.0 on 2019-07-27 00:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('institution', '0002_auto_20190726_1552'),
    ]

    operations = [
        migrations.AddField(
            model_name='institutelisting',
            name='class_name',
            field=models.CharField(default='', max_length=250),
        ),
    ]
