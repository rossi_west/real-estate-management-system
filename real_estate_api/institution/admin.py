from django.contrib import admin

# Register your models here.
from .models import InstituteListing

admin.site.register(InstituteListing)