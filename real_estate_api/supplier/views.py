from rest_framework.generics import (CreateAPIView, RetrieveUpdateAPIView,
RetrieveDestroyAPIView, ListAPIView
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import SupplierListing
from .serializer import SupplierSerializer
from .permission import IsSupplier

from accounts.authentication import JwtAuthentication
from owner.views import PropertyListingCreateAPIView

from owner.utils import upload_image_to_cloud

class CreateHotelPost(CreateAPIView):
    permission_classes = (IsAuthenticated, IsSupplier) #IsValidUser, CanPostListing)
    
    queryset = SupplierListing
    serializer_class = SupplierSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        image = upload_image_to_cloud(request.data.get('image'))
        image2 = upload_image_to_cloud(request.data.get('image2'))
        self.perform_create(serializer, image, image2)
        return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)

    def perform_create(self, serializer, image, image2):
        serializer.save(image=image, image2=image2)

class RetrieveHotelPost(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsSupplier) #IsValidUser, CanPostListing)
    
    queryset = SupplierListing
    serializer_class = SupplierSerializer
    lookup_url_kwarg = 'pk'



class DeleteHotelPost(RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated, IsSupplier) #IsValidUser, CanPostListing)
    authentication_classes = (JwtAuthentication, SessionAuthentication)
    queryset = SupplierListing
    serializer_class = SupplierSerializer
    lookup_url_kwarg = 'pk'

class ListHotelPost(ListAPIView):
    permission_classes = (IsAuthenticated, IsAuthenticated) #IsValidUser, CanPostListing)
    authentication_classes = (JwtAuthentication, SessionAuthentication)
    queryset = SupplierListing
    serializer_class = SupplierSerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(listing_id=user).all()
