from rest_framework import serializers
from accounts.models import Supplier

from .models import SupplierListing


class SupplierSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(SupplierSerializer, self).__init__(*args, **kwargs)
        request = kwargs['context']['request']
        if request.method == 'GET':
            try:
                self.Meta.exclude.remove('image')
                self.Meta.exclude.remove('image2')
            except ValueError:
                pass
            
   

    class Meta:
        model =  SupplierListing
        exclude = ['listing_id', 'location', 'image', 'image2', 'address']

    
    def create(self, validated_data):
        user = None
        request = self.context.get('request', None)
        
        
        if request and hasattr(request, 'user'):
            user = request.user
            data = self.get_services_info(user)
            return SupplierListing.objects.create(listing_id=user,
            location=data['location'],
            address=data['address'],
            **validated_data)
            


    def get_services_info(self, data):
        try:
            user = Supplier.objects.get(user=data)
        except Supplier.DoesNotExist:
            pass
        else:
            return {
                
               
                'location': user.location,
                'address': user.office_address
               
            }
        