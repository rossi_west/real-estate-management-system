from django.contrib.gis.db import models
from django.contrib.gis.geos import GEOSGeometry
from django.conf import settings

import cloudinary

from owner.utils import upload_image_to_cloud

class SupplierListing(models.Model):
    listing_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    price = models.DecimalField(max_digits=20, decimal_places=2)
    sn_measure = models.CharField(max_length=250)
    description = models.TextField()
    image = models.CharField(max_length=250, null=True)
    image2 = models.CharField(max_length=250, null=True)
    location = models.PointField(null=True, geography=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    address = models.CharField(max_length=250, default='')

    
    class Meta:
        ordering = ['-pub_date']

   

   
    

    def __str__(self):
        return self.name

    