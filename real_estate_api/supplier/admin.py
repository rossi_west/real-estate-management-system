from django.contrib import admin

# Register your models here.
from .models import SupplierListing

admin.site.register(SupplierListing)