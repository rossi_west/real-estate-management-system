from collections import OrderedDict 

from django.contrib.auth import get_user_model

from rest_framework.generics import RetrieveUpdateAPIView, CreateAPIView, ListAPIView
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes, authentication_classes

from cloudinary.uploader import upload
from cloudinary.utils import cloudinary_url

from .serializer import (UserPortFolioSerializer, CustomerS, DeveloperS, GovernmentS, \
    AgentS, SupplierS, InstituteS, HotelS, ValuerS, PropertyS)


from accounts.models import (Estate, DeveloperInfo, PropertyOwnerInfo, AgentInfo, \
    GovernmentInfo, Supplier,  Customer, Institute, HotelInfo, Valuer)
from accounts.authentication import JwtAuthentication, CsrfExemptSessionAuthentication


class UpdateUserPortFolio(RetrieveUpdateAPIView):
    
    serializer_class = UserPortFolioSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Estate
    lookup_field = 'pk'



class CreateEstate(CreateAPIView):
    
    serializer_class = UserPortFolioSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Estate
    
    def create(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context())
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)
        else:
            return Response({'reason': serializer.errors, 'res': False, 'message': 'False'}, status=status.HTTP_200_OK)

    

class ListEstate(ListAPIView):
    
    serializer_class = UserPortFolioSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Estate


    def get_queryset(self):
        return self.queryset.objects.filter(user=self.request.user)



@api_view(['POST'])
def create_portfolio(request):
    try:
        instance = Estate.objects.get(user=request.user)
    except Estate.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        serializer = UserPortFolioSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data={'res': True, 'message': 'True'}, status=status.HTTP_200_OK)
            


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def  view_profile(request):
    data =  OrderedDict()

    if request.user.is_property_owner:
       
        data['user'] = {
            'account_type': 'property owner',
            'full_name': request.user.owner.full_name,
            'contact_address': request.user.owner.contact_address,
            'gender': request.user.owner.gender,
            'profile_picture': request.user.owner.profile_picture.url,
            'user': {'email': request.user.email, 'contact_number': request.user.contact_number
            }


        }
         
    elif request.user.is_customer:
        
        data['user'] = {
            'account_type': 'customer',
            'full_name': request.user.customer.full_name,
            'contact_address': request.user.customer.contact_address,
            'profile_picture': request.user.customer.profile_picture.url,
            'user_data': {'email': request.user.email, 'contact_number': request.user.contact_number
            }


        }

    elif request.user.is_developer or request.user.is_government or request.user.is_valuer or request.user.is_supplier or request.user.is_agent:
           data['user'] = {
            'account_type': request.user.developer.service_type,
            'full_name': request.user.developer.full_name,
            'contact_address': request.user.developer.office_address,
            'profile_picture': request.user.developer.profile_picture.url,
            'user_data': {'email': request.user.email, 'contact_number': request.user.contact_number
            }

        }
      
     
    elif request.user.is_hotelier:
        data['user'] = {
            'account_type': 'hotelier',
            'full_name': request.user.hotelier.hotel_name,
            'contact_address': request.user.hotelier.address,
            'profile_picture': request.user.hotelier.profile_picture.url,
            'user_data': {'email': request.user.email, 'contact_number': request.user.contact_number
            }

        }

    elif request.user.is_institute:
            data['user'] = {
            'account_type':'institute',
            'full_name': request.user.institute.institute_name,
            'contact_address': request.user.institute.institute_address,
            'profile_picture': request.user.institute.profile_picture.url,
            'user_data': {'email': request.user.email, 'contact_number': request.user.contact_number
            }

        }

       

    return Response({'data': data,
    'message': 'true', 'reason': 'true'})



class UpdateProfile(RetrieveUpdateAPIView):
    authentication_classes = (JwtAuthentication, CsrfExemptSessionAuthentication)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'pk'

    def get_queryset(self):
        if self.request.user.is_customer:
            return Customer.objects.all()
        elif self.request.user.is_property_owner:
            return PropertyOwnerInfo.objects.all()
        elif self.request.user.is_agent:
            return AgentInfo.objects.all()
        elif self.request.user.is_developer:
            return DeveloperInfo.objects.all()
        elif self.request.user.is_institute:
            return Institute.objects.all()
        elif self.request.user.is_government:
            return GovernmentInfo.objects.all()
        elif self.request.user.is_hotelier:
            return HotelInfo.objects.all()
        elif self.request.user.is_valuer:
            return Valuer.objects.all()
        elif self.request.user.is_supplier:
            return Supplier.objects.all()

    def get_serializer_class(self):
        if self.request.user.is_customer:
            return CustomerS
        elif self.request.user.is_property_owner:
            return PropertyS
        elif self.request.user.is_agent:
            return AgentS
        elif self.request.user.is_developer:
            return DeveldoperS
        elif self.request.user.is_institute:
            return InstituteS
        elif self.request.user.is_government:
            return GovernmentS
        elif self.request.user.is_hotelier:
            return HotelS
        elif self.request.user.is_valuer:
            return ValuerS
        elif self.request.user.is_supplier:
            return SupplierS


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        try:
            image = request.data['profile_image']
        except KeyError:
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data)
            
            
        else:
            update_image = upload(
                image
            )
            url = update_image['secure_url'] #cloudinary_url(update_image['public_id'], format=update_image['format'],
           
            instance.profile_picture=url
            instance.save()
            return Response({"profile_image_url": url})
        

