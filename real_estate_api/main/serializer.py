from rest_framework import serializers

from accounts.models import Estate, Customer
from accounts.serializer import (PropertyOwnerSerializer, \
    DeveloperSerializer, AgentSerializer, GovernmentSerializer, \
        SupplierSerializer, CustomerSerializer, HotelierSerializer, InstituteSerializer, ValuerSerializer)
from accounts.models import (Estate, DeveloperInfo, PropertyOwnerInfo, AgentInfo, \
    GovernmentInfo, Supplier,  Customer, Institute, HotelInfo, Valuer)

class UserPortFolioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estate
        exclude = ('user',)

    def create(self, validated_data):
        request = self.context.get('request', None)
        user = getattr(request, 'user')
        return Estate.objects.create(user=user, **validated_data)


class CustomerS(serializers.ModelSerializer):
    class Meta:
        model = Customer
        exclude = ['user',]


class AgentS(serializers.ModelSerializer):
    class Meta:
        model = AgentInfo
        exclude = ['user',]

class PropertyS(serializers.ModelSerializer):
    class Meta:
        model = PropertyOwnerInfo
        exclude = ['user',]



class DeveloperS(serializers.ModelSerializer):
    class Meta:
        model = DeveloperInfo
        exclude = ['user',]

class InstituteS(serializers.ModelSerializer):
    class Meta:
        model = Institute
        exclude = ['user',]

class ValuerS(serializers.ModelSerializer):
    class Meta:
        model = Valuer
        exclude = ['user',]

class HotelS(serializers.ModelSerializer):
    class Meta:
        model = HotelInfo
        exclude = ['user',]


class GovernmentS(serializers.ModelSerializer):
    class Meta:
        model = GovernmentInfo
        exclude = ['user',]


class SupplierS(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        exclude = ['user',]