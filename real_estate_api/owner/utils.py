from django.conf import settings
from accounts.validators import ValidityError
import geocoder

from cloudinary.utils import cloudinary_url
from cloudinary.uploader import upload


def get_latlng(address):
    latlng = geocoder.google(location=address, key=settings.GOOGLE_API_KEY)
    if latlng:
        return  latlng
    else:
        raise ValidityError({'message': 'an error occured', 'res': False, 'reason': 
        'An error occured please verify your address'})



def upload_image_to_cloud(image_file):
    upload_url = upload(image_file, eager = [
        {'width': 400, 'height': 300, 'crop': 'pad'},
        {'width': 260, 'height': 200, 'crop': 'crop', 'gravity': 'north'}
        
    ])
    return  upload_url['secure_url']