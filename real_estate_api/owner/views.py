from rest_framework.generics import CreateAPIView, DestroyAPIView, ListAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated


from .models import PropertyListing, LandListing

from .serializer import PropertySerializer, LandSerializer
from .permission import IsValidUser, CanPostListing

from accounts.authentication import JwtAuthentication
from .utils import upload_image_to_cloud


class PropertyListingCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated, IsValidUser) #IsValidUser, CanPostListing)
    
    queryset = PropertyListing
    serializer_class = PropertySerializer

    def create(self, request, *args, **kwargs):
        context = {
            'request': request.user,
            'user_id': request.query_params.get('user_id')
        }

        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context())
        if serializer.is_valid():
            photo_one = upload_image_to_cloud(request.data.get('photo_one'))
            photo_two = upload_image_to_cloud(request.data.get('photo_two'))
            photo_three = upload_image_to_cloud(request.data.get('photo_three'))
            photo_four = upload_image_to_cloud(request.data.get('photo_four'))
            photo_five = upload_image_to_cloud(request.data.get('photo_five'))

            self.perform_create(serializer, photo_one, photo_two, photo_three, photo_four, photo_five)
            return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)
        else:
            return Response({'reason': serializer.errors, 'res': False, 'message': 'False'}, status=status.HTTP_200_OK)

    def perform_create(self, serializer, photo_one, photo_two, photo_three, photo_four, photo_five):
        serializer.save(photo_one=photo_one,  photo_two=photo_two, photo_three=photo_three, photo_four=photo_four, 
        photo_five=photo_five)

   
    
    
    
class LandListingCreateAPIView(PropertyListingCreateAPIView):
    queryset = LandListing
    serializer_class = LandSerializer

    def create(self, request, *args, **kwargs):
        context = {
            'request': request.user,
            'user_id': request.query_params.get('user_id')
        }

        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context())
        if serializer.is_valid():
            land_photo = upload_image_to_cloud(request.data.get('land_photo'))
            photo_one = upload_image_to_cloud(request.data.get('photo_one'))
            photo_two = upload_image_to_cloud(request.data.get('photo_two'))
            photo_three = upload_image_to_cloud(request.data.get('photo_three'))
            photo_four = upload_image_to_cloud(request.data.get('photo_four'))
            photo_five = upload_image_to_cloud(request.data.get('photo_five'))

            self.perform_create(serializer, land_photo, photo_one, photo_two, photo_three, photo_four, photo_five)
            return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)
        else:
            return Response({'reason': serializer.errors, 'res': False, 'message': 'False'}, status=status.HTTP_200_OK)

    def perform_create(self, serializer, land_photo, photo_one, photo_two, photo_three, photo_four, photo_five):
        serializer.save(land_photo=land_photo, photo_one=photo_one,  photo_two=photo_two, photo_three=photo_three, photo_four=photo_four, 
        photo_five=photo_five)


class ListPropertyAPIView(ListAPIView):
    permission_classes = (IsAuthenticated, IsValidUser) #, CanPostListing)
   
    queryset = PropertyListing
    serializer_class = PropertySerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(listing_id=user).all()


class ListLandAPIView(ListPropertyAPIView):
    queryset = LandListing
    serializer_class = LandSerializer

    
class UpdatePropertyAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsValidUser) #IsValidUser, CanPostListing)
    authentication_classes = (JwtAuthentication, SessionAuthentication)
    serializer_class = PropertySerializer
    queryset = PropertyListing
    lookup_url_kwarg = 'pk'


class UpdateLandAPIView(UpdatePropertyAPIView):
    serializer_class = LandSerializer
    queryset = LandListing


class DeletePropertyAPIView(DestroyAPIView):
    queryset = PropertyListing
    serializer_class = PropertySerializer
    permission_classes = (IsAuthenticated, IsValidUser) #IsValidUser, CanPostListing)
    
    lookup_url_kwarg = 'pk'


class DeleteLandAPIView(DeletePropertyAPIView):
    queryset = PropertyListing
    serializer_class = PropertySerializer
