from django.contrib.auth import get_user_model

from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError



from accounts.models import PropertyOwnerInfo, AgentInfo

from .models import PropertyListing, LandListing

from .utils import upload_image_to_cloud



class PropertySerializer(ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(PropertySerializer, self).__init__(*args, **kwargs)
        request = kwargs['context']['request']
        if  request.method == 'GET':
            try:
                self.Meta.exclude.remove('photo_one')
                self.Meta.exclude.remove('photo_two')
                self.Meta.exclude.remove('photo_three')
                self.Meta.exclude.remove('photo_four')
                self.Meta.exclude.remove('photo_five')
                self.Meta.exclude.remove('land_photo')
            except:
                pass
            
            print(self.Meta.exclude)

    
    class Meta:
        model = PropertyListing
        exclude = ['listing_id', 'location', 'contact_name',
        'contact_profile_photo',  'photo_one', 'photo_two', 
        
        'photo_three',
        
        'photo_four', 'photo_five',
        ]

    def create(self, validated_data):
        request = self.context.get('request', None)
        

        #user_id = self.context.get('user_id', None)

        if request is not None and hasattr(request, 'user'):
            user = request.user
            contact_info = self.get_user_photo(user)
            create_property_listing = PropertyListing.objects.create(
                listing_id=user, 
                contact_name=contact_info['full_name'], 
                contact_profile_photo=contact_info['image'],
                 **validated_data)

            return create_property_listing



    def get_user_photo(self, obj):
        if obj.is_property_owner:
            try:
                info = PropertyOwnerInfo.objects.get(user=obj)
            except PropertyOwnerInfo.DoesNotExist:
                pass
            else:
                return {'full_name': info.full_name, 'image': info.profile_picture}
        else:
            try:
                info = AgentInfo.objects.get(user=obj)
            except AgentInfo.DoesNotExist:
                pass
            else:
                return {'full_name': info.full_name, 'image': info.profile_picture}



class LandSerializer(PropertySerializer):
    
    class Meta:
        model = LandListing
        exclude = ['listing_id', 'location',
        'contact_name',
        'contact_profile_photo',  
        'land_photo',
        'photo_one', 
        'photo_two', 
        'photo_three',
        'photo_four', 
        'photo_five'
       
        ]

    def create(self, validated_data):
        #address = validated_data.pop('address')
        #city = validated_data.pop('city')
        #full_address = '{}, {}'.format(address, city)
        request = self.context.get('request', None)
        #contact_info = self.get_user_photo(user)
        #user_id = self.context.get('user_id', None)

        if request is not None and hasattr(request, 'user'):
            user = request.user
            contact_info = self.get_user_photo(user)
            
            
            create_property_listing = LandListing.objects.create(listing_id=user, 
            contact_name=contact_info['full_name'],
            contact_profile_photo=contact_info['image'],
            
            **validated_data)
            return create_property_listing
       
        else:
            raise ValidationError(detail='failed')


