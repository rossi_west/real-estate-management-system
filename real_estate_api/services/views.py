from django.shortcuts import render

# Create your views here.
from rest_framework.generics import (CreateAPIView, RetrieveUpdateAPIView,
RetrieveDestroyAPIView, ListAPIView
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication

from .models import ServicesListing
from .serializer import ServicesSerializer
from .permission import IsServices
from accounts.authentication import JwtAuthentication

from rest_framework.permissions import IsAuthenticated
from owner.views import PropertyListingCreateAPIView

from owner.utils import upload_image_to_cloud




class CreateHotelPost(CreateAPIView):
    permission_classes = (IsAuthenticated, IsServices
    
    )
    
    queryset = ServicesListing
    serializer_class = ServicesSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        image = upload_image_to_cloud(request.data.get('image'))
        self.perform_create(serializer, image)
        return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)

    def perform_create(self, serializer, image):
        serializer.save(image=image)

        

class RetrieveHotelPost(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsServices
    
    )
    

    queryset = ServicesListing
    serializer_class = ServicesSerializer
    lookup_url_kwarg = 'pk'



class DeleteHotelPost(RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated, IsServices
    
    ,)
    authentication_classes = (JwtAuthentication, SessionAuthentication)
    queryset = ServicesListing
    serializer_class = ServicesSerializer
    lookup_url_kwarg = 'pk'

class ListHotelPost(ListAPIView):
    permission_classes = (IsAuthenticated, IsServices
    
    ,)
    
    queryset = ServicesListing
    serializer_class = ServicesSerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(services_id=user).all()
