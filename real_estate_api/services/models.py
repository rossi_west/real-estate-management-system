from django.contrib.gis.db import models
from django.contrib.gis.geos import GEOSGeometry
from django.conf import settings
from owner.utils import upload_image_to_cloud

import cloudinary

# Create your models here.
class ServicesListing(models.Model):
    services_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    description = models.TextField()
    topic = models.CharField(max_length=250)
    image = models.CharField(max_length=250, null=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    location = models.PointField(null=True, geography=True)
    order_uuid = models.CharField(max_length=250, default='')

    class Meta:
        ordering = ['-pub_date']




 
    def __str__(self):
        return self.description


