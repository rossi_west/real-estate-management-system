import random 
from rest_framework import serializers

from .models import HotelListing
from accounts.models import HotelInfo
from owner.utils import upload_image_to_cloud


def generated_unique_id():
    ran = ''.join(str(random.randint(2, 8)) for x in range(6))
    return ran




class CreateHotelPostSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(CreateHotelPostSerializer, self).__init__(*args, **kwargs)
        request = kwargs['context']['request']
        if  request.method == 'GET':
            try:
                self.Meta.exclude.remove('image_one')
                self.Meta.exclude.remove('image_two')
                self.Meta.exclude.remove('image_three')
                self.Meta.exclude.remove('image_four')
               
            except ValueError:
                pass
            
            print(self.Meta.exclude)
    

    class Meta:
        model = HotelListing
        exclude = ['website', 'hotel_name',  'listing_id', 'address',
        'city', 'order_uuid', 'location',
        'state', 'image_one', 'image_two', 'image_three', 'image_four'
        ]
    
 
    def create(self, validated_data):
        user = None
        request = self.context.get('request', None)
        
        if request and hasattr(request, 'user'):
            
            user = request.user
            hotel_info = self.get_hotel_info(user)
            
    
       
         

            hotel_post = HotelListing.objects.create(listing_id=user,
            order_uuid=hotel_info['order_id'],
           
            **validated_data)
            return hotel_post

    def get_hotel_info(self, data):
        try:
            user = HotelInfo.objects.get(user=data)
        except HotelInfo.DoesNotExist:
            pass
        else:
            return {
                'website': user.hotel_website,
                'address': user.address,
                'city': user.city,
                'hotel_name': user.hotel_name,
                'state':user.state,
                'location': user.location,
                'order_id': user.order_id
            }
        

    
   


