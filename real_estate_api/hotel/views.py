
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .models import HotelListing
from .serializer import CreateHotelPostSerializer
from accounts.authentication import JwtAuthentication
from .permission import IsHotel
from owner.views import PropertyListingCreateAPIView, ListPropertyAPIView, UpdatePropertyAPIView, DeletePropertyAPIView
from owner.utils import upload_image_to_cloud

class CreateHotelPost(PropertyListingCreateAPIView):
    permission_classes = (IsAuthenticated,) #IsValidUser, CanPostListing)
    
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer

    def create(self, request, *args, **kwargs):
        serializer  = self.get_serializer(data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        image_one = upload_image_to_cloud(request.data.get('image_one'))
        image_two = upload_image_to_cloud(request.data.get('image_two'))
        image_three = upload_image_to_cloud(request.data.get('image_three'))
        image_four = upload_image_to_cloud(request.data.get('image_four')) 
        self.perform_create(serializer, image_one, image_two, image_three, image_four)
        return Response({'reason': 'created successfully', 'res': True, 'message': 'True'}, status=status.HTTP_200_OK)

    def perform_create(self,  serializer, image_one, image_two, image_three, image_four):
        serializer.save(image_one=image_one, image_two=image_two, image_three=image_three, image_four=image_four)
        

class RetrieveHotelPost(UpdatePropertyAPIView):
    permission_classes = (IsAuthenticated, IsHotel) #IsValidUser, CanPostListing)
    
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer
    lookup_url_kwarg = 'pk'


class DeleteHotelPost(DeletePropertyAPIView):
    permission_classes = (IsAuthenticated, IsHotel) #IsValidUser, CanPostListing)
    
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer
    lookup_url_kwarg = 'pk'


class ListHotelPost(ListPropertyAPIView):
    permission_classes = (IsAuthenticated,IsHotel) #IsValidUser, CanPostListing)
    
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer
