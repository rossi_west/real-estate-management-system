from django.contrib.auth import get_user_model
from rest_framework.serializers import ModelSerializer

from owner.models import PropertyListing, LandListing
from accounts.models import HotelInfo, ServicesInfo, Supplier, Institute
from hotel.models  import HotelListing
from supplier.models import SupplierListing
from professional.models import ProfessionalListing
from services.models import ServicesListing
from institution.models  import InstituteListing


class PropertySerializer(ModelSerializer):
    class Meta:
        model = PropertyListing
        exclude = ('location', 'latitude', 'longitude')


class Hotel(ModelSerializer):
    class Meta:
        model = HotelInfo
        exclude = ('latitude', 'longitude', 'work_identity', 'profile_picture',
        'cac_number', 'location')

    


class HotelSerializer(ModelSerializer):
    class Meta:
        model = HotelListing
        fields = '__all__'

class LandSerializer(ModelSerializer):
    class Meta:
        model = LandListing
        exclude = ('location', 'latitude', 'longitude')


class ProfessionalSerializer(ModelSerializer):
    class Meta:
        model = ProfessionalListing
        exclude = ('location', 'latitude', 'longitude')



class SupplierSerializer(ModelSerializer):
    class Meta:
        model = Supplier
        exclude = ('full_name', 'office_address', 'profile_picture')


class ServicesSerializer(ModelSerializer):
    class Meta:
        model = ServicesInfo
        exclude = ('location', 'work_identity')

class ServicesInfoSerializer(ModelSerializer):
    class Meta:
        model = ServicesListing
        fields = '__all__'

class SupplierListingSerializer(ModelSerializer):
    class Meta:
        model =  SupplierListing
        fields = '__all__'

class InstituteSerializer(ModelSerializer):
    class Meta:
        model = Institute
        fields = ('user', 'institute_name', 'institute_address', 'website', 'profile_picture', 'order_id', )

class InstituteInfoSerializer(ModelSerializer):
    class Meta:
        model = InstituteListing
        fields = ('institute_id', 'order_uuid', 'description', 'data', 'created', 'class_name')

