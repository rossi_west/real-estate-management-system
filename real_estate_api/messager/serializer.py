from rest_framework import serializers
from accounts.utils import account_full_name, account_image

from .models import Message


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

    def to_representation(self, instance):
        ret = super(ChatSerializer, self).to_representation(instance)
        request = self.context.get('request')
        print(request.user)

        
        if instance.sender == request.user:
            receipent_name = account_full_name(instance.recipient)
            receipent_photo =account_image(instance.recipient)
            ret['recipient_chat_name'] = receipent_name['account_name']
            ret['recipient_profile_picture'] = receipent_photo
            return ret
        else:
            receipent_name = account_full_name(instance.sender)
            receipent_photo =account_image(instance.sender)
            ret['sender_chat_name'] = receipent_name['account_name']
            ret['sender_profile_picture'] = receipent_photo
            return ret


class ChatListingSerializer(ChatSerializer):
    class Meta:
        model = Message
        exclude = ('message', )


class Chat(serializers.Serializer):
    to = serializers.IntegerField()
    message = serializers.CharField(max_length=200)