from django.urls  import path
from django.views.decorators.csrf import csrf_exempt

from . import views

app_name = 'messager'
urlpatterns = [
    path('', views.MessagesListView.as_view(), name='messages_list'),
    path('unread/', views.UnreadMessagesListView.as_view(), name='unread_messages_list'),
    path('send-message/', csrf_exempt(views.SendMessage.as_view()), name='send_message'),
    path('receive-message/',
        views.receive_message, name='receive_message'),
    path('<int:pk>/', views.ConversationListView.as_view(),
        name='conversation_detail'),
]
