
from .base import *

import dj_database_url
from decouple import config



DATABASES = {}

DATABASES['default'] =  dj_database_url.config(default=config('DATABASE_URL', default="postgres://hbttsdesfzeyzf:45c03ff7d5487e866b9d4df49d6e132976d82603ac3bbe683c01c1e96d4b07e3@ec2-50-16-241-91.compute-1.amazonaws.com:5432/dfhpn8t99im8o5"))
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'

"""

GEOS_LIBRARY_PATH = "{}/libgeos_c.so".format(os.environ.get('GEOS_LIBRARY_PATH'))
GDAL_LIBRARY_PATH = "{}/libgdal.so".format(os.environ.get('GDAL_LIBRARY_PATH'))
PROJ4_LIBRARY_PATH = "{}/libproj.so".format(os.environ.get('PROJ4_LIBRARY_PATH'))

"""
GDAL_LIBRARY_PATH = "/app/.heroku/vendor/lib/libgdal.so"
GEOS_LIBRARY_PATH = "/app/.heroku/vendor/lib/libgeos_c.so"
