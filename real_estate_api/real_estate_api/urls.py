"""real_estate_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('', include('main.urls')),
    path('property/', include('owner.urls')),
    path('hotel/', include('hotel.urls')),
    path('customer/', include('customer.urls')),
    path('owner/', include('owner.urls')),
    path('supplier/', include('supplier.urls')),
    path('professional/', include('professional.urls')),
    path('government/', include('government.urls')),
    path('valuer/', include('valuer.urls')),
    path('developer/', include('developer.urls')),
    path('services/', include('services.urls')),
    path('blog/', include('blog.urls')),
    path('notification/', include('notification.urls')),
    path('messenger/', include('messager.urls')),
    path('institution/', include('institution.urls')) 
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
